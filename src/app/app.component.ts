import {Component, OnInit} from '@angular/core';
import {MessageItem, MessageService} from "./shared/message/message.service";
import {LoaderService} from "./shared/loader/loader.service";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {LoaderDialogComponent} from "./layout/loader-dialog/loader-dialog.component";
import {ConfirmDialogComponent} from "./layout/confirm/confirm-dialog.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {


  private dialogRef: MatDialogRef<any>;


  /**
   * Construtor da classe.
   *
   * @param loaderService
   * @param messageService
   * @param dialog
   */
  constructor(
    private loaderService: LoaderService,
    private messageService: MessageService,
    private dialog: MatDialog
  ) {
  }

  /**
   * Inicializa o Component.
   */
  ngOnInit(): void {

    this.loaderService.onStart.subscribe(() => {
      this.dialogRef = this.dialog.open(LoaderDialogComponent, {
        minWidth: '50px',
        minHeight: '50px',
        hasBackdrop: true,
        disableClose: true
      });
    });

    this.loaderService.onStop.subscribe(() => {
      this.dialogRef.close();
    });

    this.messageService.getMsgEmitter().subscribe((item: MessageItem) => this.addMsgItem(item));
    this.messageService.getConfirmEmitter().subscribe((item: MessageItem) => this.addConfirmItem(item));
  }

  /**
   * Adiciona o item de mensagem a visualização.
   *
   * @param item
   */
  private addMsgItem(item: MessageItem): void {
    this.messageService.addConfirmOk(item.msg);
  }

  /**
   * Adiciona o modal de confirmação a view.
   *
   * @param item
   */
  private addConfirmItem(item: MessageItem): void {
    this.dialog.open(ConfirmDialogComponent, {
      minWidth: '30%',
      minHeight: '30%',
      disableClose: true,
      data: {item}
    });
  }
}
