import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule } from '@angular/material/dialog';

import { ConfirmDialogComponent } from './confirm-dialog.component';
import { MessageModule } from 'src/app/shared/message/message.module';

/**
 * Modulo de dialog de confirmação.
 */
@NgModule({
  declarations: [ConfirmDialogComponent],
  entryComponents: [ConfirmDialogComponent],
  imports: [
    CommonModule,
    MessageModule,
    MatDialogModule,
    FlexLayoutModule
  ]
})
export class ConfirmModule { }
