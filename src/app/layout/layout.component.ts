import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {HeaderComponent} from "./header/header.component";
import {ManagementEventService} from "../shared/services/management-event.service";

/**
 * @author GF
 */
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  queries: {
    viewContent: new ViewChild('viewContent', {static: true})
  },
})
export class LayoutComponent implements AfterViewInit {

  @ViewChild('header', {static: true}) header: HeaderComponent;

  public viewContent: ElementRef;

  ngAfterViewInit(): void {

  }

  constructor(
    private eventService: ManagementEventService) {
  }

}
