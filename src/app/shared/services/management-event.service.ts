/* tslint:disable:no-redundant-jsdoc variable-name callable-types */
import {Injectable, EventEmitter} from '@angular/core';

/**
 * Classe de representação de 'Mensagem'.
 *
 * @author GF
 */
export class EventMessage {
  public static ON_RELATORIO_DADOS_COMUM_SAVE = 'relatorio_dados_comum_save';

  /**
   * Construtor da classe.
   *
   * @param code
   * @param message
   */
  constructor(
    public code?: string,
    public message?: string
  ) {
  }
}

/**
 * Interface 'Listener' que determina o contrato da função callback referente ao 'confirm'.
 *
 * @author GF
 */
export interface EventMessageListener {
  (): void;
}

/**
 * Classe de representação de 'Item de Mensagem'.
 *
 * @author GF
 */
export class EventMessageItem {

  private _message: EventMessage;
  private _eventListener: EventMessageListener;

  /**
   * Construtor da classe.
   *
   * @param message
   * @param listenerListener
   */
  constructor(message: EventMessage, listenerListener?: EventMessageListener) {
    this._message = message;
    this._eventListener = listenerListener;
  }

  /**
   * @returns msg
   */
  public get message(): EventMessage {
    return this._message;
  }


  /**
   * Execulta o callback para as ações 'OK/YES'.
   */
  public executeEventAction(): void {
    if (this._eventListener !== null && this._eventListener !== undefined) {
      this._eventListener();
    }
  }

}

/**
 * Classe 'service' responsável por prover o recurso de mensagem da aplicação.
 *
 * @author GF
 */
@Injectable()
export class ManagementEventService {
  private eventEmitter: EventEmitter<EventMessage>;
  private eventMessageItens: EventMessageItem[];

  /**
   * Construtor da classe.
   */
  constructor() {
    this.eventEmitter = new EventEmitter();
    this.eventEmitter.subscribe((eventMessage: EventMessage) => {
      this.executeEventHandler(eventMessage);
    });
    this.eventMessageItens = [];
  }

  /**
   * Localiza os iten de eventos registrados para executar..
   * @param evtMessageItem
   */
  private executeEventHandler(evtMessageItem: EventMessage) {
    this.eventMessageItens.forEach((evtMessage: EventMessageItem) => {
      if (evtMessage.message.code === evtMessageItem.code) {
        evtMessage.executeEventAction();
      }
    });
  }

  /**
   * registrar um evento para ser executado.
   * @param eventCode
   * @param eventCallBack
   */
  public registerEvent(eventCode: string, eventCallBack: EventMessageListener) {
    const eventMessageItem = new EventMessageItem(new EventMessage(EventMessage.ON_RELATORIO_DADOS_COMUM_SAVE), eventCallBack);
    this.eventMessageItens.push(eventMessageItem);
  }

  /**
   * Emite o evento para mensagem informado.
   *
   * @param data
   */
  public emitEvent(data: EventMessage): void {
    this.eventEmitter.emit(data);
  }


  /**
   * @returns EventEmitter
   */
  public getEventEmitter(): EventEmitter<EventMessage> {
    return this.eventEmitter;
  }

}
