import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'astronauta',
        loadChildren: () => import('./astronauta/astronauta.module').then(module => module.AstronautaModule),
    },
    {
        path: 'planeta',
        loadChildren: () => import('./planeta/planeta.module').then(module => module.PlanetaModule),
    }
];

/**
 * Configuração global de rotas.
 *
 * @author GF
 */
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule {
}
