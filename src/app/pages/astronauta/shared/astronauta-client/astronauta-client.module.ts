import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AstronautaClientService} from './astronauta-client.service';
import {AstronautaResolve} from "./astronauta.resolve";


/**
 * Modulo de integração do projeto frontend com os serviços backend.
 */
@NgModule({
    declarations: [],
    imports: [
        CommonModule,
    ],
    providers: [
        AstronautaClientService,
        AstronautaResolve
    ]
})
export class AstronautaClientModule {
}
