import {Component, OnInit} from '@angular/core';
import {MessageService} from "../../../shared/message/message.service";
import {NgForm} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AstronautaClientService} from "../shared/astronauta-client/astronauta-client.service";

@Component({
    selector: 'app-astronauta-list',
    templateUrl: './astronauta-list.component.html'
})
export class AstronautaListComponent implements OnInit {

    public astronauta: any = {};

    public astronautas: any = [];

    /**
     * Construtor da classe
     *
     * @param route
     * @param router
     * @param messageService
     * @param astronautaClientService
     */
    constructor(
        route: ActivatedRoute,
        private router: Router,
        private messageService: MessageService,
        private astronautaClientService: AstronautaClientService
    ) {
    }

    ngOnInit() {
        this.pesquisar(this.astronauta)
    }

    /**
     * Responsável por pesquisar os itens.
     *
     * @param astronauta
     * @param form
     */
    public pesquisar(astronauta: any) {
        this.astronautaClientService.filtrar(astronauta).subscribe((data) => {
            this.astronautas = data.result
        }, error => {
            this.messageService.addMsgDanger(error.errorMessage);
        });

    }

    /**
     * Responsável por excluir um item.
     *
     * @param astronauta
     */
    public excluir(astronauta: any): void {
        this.messageService.addConfirmYesNo('MSG036', () => {
            this.astronautaClientService.excluir(astronauta).subscribe(() => {
                this.pesquisar(this.astronauta);
                this.messageService.addMsgSuccess("MSG003");
            }, error => {
                this.messageService.addMsgDanger(error);
            });
        });
    }


}
