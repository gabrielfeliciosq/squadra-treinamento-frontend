import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {MessageService} from "../../../shared/message/message.service";
import {AstronautaClientService} from "../shared/astronauta-client/astronauta-client.service";
import {AcaoSistema} from "../../../shared/component/acao-sistema.acao";

@Component({
    selector: 'app-astronauta-form',
    templateUrl: './astronauta-form.component.html'
})
export class AstronautaFormComponent implements OnInit {

    public astronauta: any;

    public acaoSistema: AcaoSistema;

    /**
     * Construtor da classe
     *
     * @param route
     * @param router
     * @param messageService
     * @param astronautaClientService
     */
    constructor(
        route: ActivatedRoute,
        private router: Router,
        private messageService: MessageService,
        private astronautaClientService: AstronautaClientService
    ) {

        this.acaoSistema = new AcaoSistema(route);

        if (this.acaoSistema.isAcaoIncluir()) {
            this.astronauta = {}
        } else {
            this.astronauta = route.snapshot.data.astronauta;
        }

    }

    ngOnInit() {
    }

    /**
     * Responsável por salvar.
     *
     * @param astronauta
     * @param form
     */
    public salvar(astronauta: any, form: NgForm) {
        if (form.valid) {
            astronauta.idade = parseInt(astronauta.idade);

            this.astronautaClientService.salvar(astronauta).subscribe(() => {
                this.messageService.addMsgSuccess('MSG003');
                this.router.navigateByUrl('astronauta/listar');
            }, error => {
                this.messageService.addMsgDanger(error.errorMessage);
            });
        }
    }


}
