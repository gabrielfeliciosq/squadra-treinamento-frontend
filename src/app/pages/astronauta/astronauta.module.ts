import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AstronautaFormComponent} from "./astronauta-form/astronauta-form.component";
import {AstronautaListComponent} from "./astronauta-list/astronauta-list.component";
import {AstronautaRoutes} from "./astronauta.router";
import {RouterModule} from "@angular/router";
import {MatCardModule} from "@angular/material/card";
import {FlexModule} from "@angular/flex-layout";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {FormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MessageModule} from "../../shared/message/message.module";
import {AstronautaClientModule} from "./shared/astronauta-client/astronauta-client.module";
import {CdkTableModule} from "@angular/cdk/table";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";

/**
 * @author GF
 */
@NgModule({
    declarations: [AstronautaFormComponent, AstronautaListComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(AstronautaRoutes),
        MatCardModule,
        FlexModule,
        MatFormFieldModule,
        AstronautaClientModule,
        MatSelectModule,
        FormsModule,
        MatButtonModule,
        MatInputModule,
        MessageModule,
        CdkTableModule,
        MatTableModule,
        MatIconModule
    ]
})
export class AstronautaModule {
}
