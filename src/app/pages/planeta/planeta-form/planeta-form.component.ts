import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {MessageService} from "../../../shared/message/message.service";
import {PlanetaClientService} from "../shared/planeta-client/planeta-client.service";
import {AcaoSistema} from "../../../shared/component/acao-sistema.acao";

@Component({
    selector: 'app-planeta-form',
    templateUrl: './planeta-form.component.html'
})
export class PlanetaFormComponent implements OnInit {

    public planeta: any;

    public acaoSistema: AcaoSistema;

    /**
     * Construtor da classe
     *
     * @param route
     * @param router
     * @param messageService
     * @param planetaClientService
     */
    constructor(
        route: ActivatedRoute,
        private router: Router,
        private messageService: MessageService,
        private planetaClientService: PlanetaClientService
    ) {

        this.acaoSistema = new AcaoSistema(route);

        if (this.acaoSistema.isAcaoIncluir()) {
            this.planeta = {}
        } else {
            this.planeta = route.snapshot.data.planeta;
        }

    }

    ngOnInit() {
    }

    /**
     * Responsável por salvar.
     *
     * @param planeta
     * @param form
     */
    public salvar(planeta: any, form: NgForm) {
        if (form.valid) {
            this.planetaClientService.salvar(planeta).subscribe(() => {
                this.messageService.addMsgSuccess('MSG003');
                this.router.navigateByUrl('planeta/listar');
            }, error => {
                this.messageService.addMsgDanger(error.errorMessage);
            });
        }
    }


}
