import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, Router} from "@angular/router";

import {MessageService} from 'src/app/shared/message/message.service';
import {PlanetaClientService} from "./planeta-client.service";

/**
 * Classe resolve responsável pela busca das informações do Astronauta conforme o id.
 *
 * @author Squadra Tecnologia
 */
@Injectable()
export class PlanetaResolve implements Resolve<any> {

    /**
     * Construtor da classe.
     *
     * @param router
     * @param planetaClientService
     * @param messageService
     */
    constructor(
        private router: Router,
        private planetaClientService: PlanetaClientService,
        private messageService: MessageService
    ) {
    }

    /**
     * @param route
     */
    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        let id = route.params["id"];

        return new Observable(observer => {
            this.planetaClientService.obterPorId(id).subscribe(
                data => {
                    observer.next(data);
                    observer.complete();
                },
                error => {
                    observer.error(error.errorMessage);
                    this.router.navigate([""]);
                    this.messageService.addMsgDanger(error.errorMessage);
                }
            );
        });
    }
}
